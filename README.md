# Project description:
Main point for this react app is to demonstrate abilities in following topics:
- basic knowledge with typescript
- work with async operations
- write unit tests
- style components with css

# Installation steps:

As simple as that:
``` 
npm install
npm start
```

# 3rd party integrations:
- https://restcountries.com
- https://countryflagsapi.com

# Todo:
- [x] Sort data by predefined criteria
- [ ] Write additional tests
- [ ] Make responsive view
- [ ] Add better error handling