import {URL_FLAG_API} from  '../configs/constants'
export const flagURL=(country:string|undefined)=>{
    if(country) {
        return `${URL_FLAG_API}/${country.toLowerCase()}`
    }
}