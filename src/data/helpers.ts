import { newUnit } from "../data/jsonDeserialize";
import axios from "axios";

export const sortLastOnline = (data: newUnit[]) => [
  ...data.sort((a, b) => b.lastOnline.valueOf() - a.lastOnline.valueOf()),
];

export const sortLastUpdated = (data: newUnit[]) => [
  ...data.sort((a, b) => b.lastUpdated.valueOf() - a.lastUpdated.valueOf()),
];

export const sortBySerial = (data: newUnit[]) => [
  ...data.sort((a, b) => Number(b.serialNumber) - Number(a.serialNumber)),
];

export const getLastOnline = (data: newUnit[]) => {
  const lastElement = sortLastOnline(data).at(0);
  const serialNumber = lastElement?.serialNumber;
  const lastOnline = lastElement?.lastOnline;
  return serialNumber
    ? `Last online client:${lastOnline?.toUTCString()} sn:${serialNumber}`
    : "";
};

export const getLastUpdated = (data: newUnit[]) => {
  const lastElement = sortLastUpdated(data).at(0);
  const serialNumber = lastElement?.serialNumber;
  const lastUpdated = lastElement?.lastUpdated;
  return serialNumber
    ? `Last updated client:${lastUpdated?.toUTCString()} sn:${serialNumber}`
    : "";
};

export const countCountries = (data: newUnit[]) => {
  const coutries = new Set();
  data.forEach((record) => coutries.add(record.countryCode));
  return coutries.size;
};

export const searchSerial = (data: newUnit[], serial: string) => {
  return data.filter((record) => record.serialNumber.startsWith(serial));
};

export const fetchCountryInfo = async (code: string = "dk") => {
  const req = await axios.get(
    `https://restcountries.com/v2/alpha?codes=${code}`
  );
  const [result] = await req.data;
  return {
    name: result.name,
    capital: result.capital,
    region: result.region,
    population: result.population,
    flag: result.flag,
  };
};

