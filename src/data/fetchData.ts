import  MockedData from './dataset.json';

export const fetchData=():Promise<Object[]>=>new Promise((res)=> {
    return setTimeout(() => res([...MockedData]), 5000)
})