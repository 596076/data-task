export interface dataUnit{
    serialNumber: string
    currentSoftwareVersion: string
    lastUpdated: string
    level: string
    context: string
    countryCode?: string
    productCountryCode?: string
    productTags?: string
    component?: string
    lastOnline: string
    slidingHistoryFlakinessDetected: boolean
}

// extends and overwrite some property types ( properties related to date and numbers )
export interface newUnit extends Omit<dataUnit,"lastOnline"|"lastUpdated">{
    lastOnline:Date
    lastUpdated:Date

}

// transform some properties in appropriate type
export const parseData=(data:dataUnit)=>{
    const tempData:dataUnit=JSON.parse(JSON.stringify(data))
    const createNewObj:newUnit = {
        ...tempData,
        lastOnline : new Date(tempData.lastOnline),
        lastUpdated : new Date(tempData.lastUpdated)
    }
    return(createNewObj)
}

// MockedData.forEach((i:dataUnit) => {
//     console.log(parseData(i))
// })