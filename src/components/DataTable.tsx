import { Dispatch, SetStateAction, useEffect, useState } from "react";
import { fetchData } from "../data/fetchData";
import { dataUnit, newUnit, parseData } from "../data/jsonDeserialize";
import { flagURL } from "../data/flagAPI";
import "./DataTable.css";
import { fetchCountryInfo } from "../data/helpers";

interface ITable {
  dataProp: newUnit[];
  setIsModalOpen: Dispatch<SetStateAction<boolean>>;
  fetchCountryApi: (arg: string) => void;
}

export const DataTable: React.FC<ITable> = ({
  dataProp,
  setIsModalOpen,
  fetchCountryApi,
}) => {
  const data = [...dataProp];
  return (
    <table className="record">
      <thead>
        <tr>
          <th>serial num</th>
          <th>software ver</th>
          <th>last Online</th>
          <th>last Updated</th>
          <th>status</th>
          <th>country</th>
        </tr>
      </thead>
      <tbody>
        {data.length > 0 ? (
          data.map((record, index) => (
            <tr key={index}>
              <td>{record.serialNumber}</td>
              <td>{record.currentSoftwareVersion}</td>
              <td>{record.lastOnline.toUTCString()}</td>
              <td>{record.lastUpdated.toUTCString()}</td>
              <td>{record.level}</td>
              <td>
                {"countryCode" in record && (
                  <img
                    src={flagURL(record.productCountryCode?.toLowerCase())}
                    alt={record.productCountryCode?.toLowerCase()}
                    title={record.productCountryCode?.toLowerCase()}
                    onClick={() => {
                      document.body.style.overflowY = "hidden";
                      fetchCountryApi(
                        record.productCountryCode?.toLowerCase()!
                      );
                      setIsModalOpen(true);
                    }}
                  />
                )}
              </td>
            </tr>
          ))
        ) : (
          <tr>
            <td colSpan={6} style={{ textAlign: "center" }}>
              Loading data...
            </td>
          </tr>
        )}
      </tbody>
    </table>
  );
};
