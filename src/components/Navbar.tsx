import React from "react";
import "./Navbar.css"
export const Navbar:React.FC<{ children: any }>=(props)=>{
    const {children} = props
    return (
        <div className='navbar--view'>
            {children}
        </div>
    )
}