import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import { Navbar } from "../components/Navbar";
import { Card } from "../components/Card";
import { DataTable } from "../components/DataTable";
import "./Main.css";
import { newUnit, parseData } from "../data/jsonDeserialize";
import { fetchData } from "../data/fetchData";
import {
  countCountries,
  getLastOnline,
  getLastUpdated,
  sortLastUpdated,
  sortLastOnline,
  sortBySerial,
  searchSerial,
  fetchCountryInfo,
} from "../data/helpers";
import { Dropdown } from "./Dropdown";
import { Modal } from "./Modal";

interface IMain {
  setLoading: Dispatch<SetStateAction<boolean>>;
}

export const Main: React.FC<IMain> = (props: IMain) => {
  const { setLoading } = props;
  window.addEventListener("load", () => {
    setLoading(false);
  });

  // All fetched data
  const [data, setData] = useState<newUnit[]>([]);
  // Only selected newUnit[] data from dropdown
  const [selectData, setSelectData] = useState<newUnit[]>([]);
  // selected string option from dropdown menu
  const [selection, setSelection] = useState("");

  const [isModalOpen, setIsModalOpen] = useState(false);

  interface ICountry {
    name: string;
    capital: string;
    region: string;
    population: number;
    flag: string;
  }
  const [countryInfo, setCountryInfo] = useState<ICountry>();
  const [isCountryDataLoaded, setCountryDataLoaded] = useState(false);

  useEffect(() => {
    setSelectData(data);
  }, [data]);

  useEffect(() => {
    switch (selection) {
      case "serial":
        setSelectData([...sortBySerial(data).slice(0, 20)]);
        break;
      case "online":
        setSelectData([...sortLastOnline(data).slice(0, 10)]);
        break;
      case "updated":
        setSelectData([...sortLastUpdated(data)]);
        break;
    }
  }, [selection]);

  const fetchCountryApi = (country: string) => {
    return fetchCountryInfo(country).then(async (data) => {
      await setCountryInfo({ ...data });
      await console.log("data fetched");
      await setCountryDataLoaded(true);
    });
  };

  useEffect(() => {
    const tempArr: newUnit[] = [];
    fetchData()
      .then((data) =>
        data.forEach((record: any, index: number) => {
          tempArr.push(parseData(record));
        })
      )
      .then(() => {
        setData([...tempArr]);
        const target = document.querySelector(".record");

        const modalClose = document.getElementById("mainRootModal");
        modalClose?.addEventListener("click", (e) => {
          const element = e.target as HTMLElement;

          if (!element.closest("#modal-wrapper")) {
            setIsModalOpen(false);
            document.body.style.overflowY = "scroll";
          }
        });
      });
  }, []);

  return (
    <div id="main--wrapper">
      <section id="main--body">
        <Navbar>
          <Card
            img={"http://#"}
            title={"Last Updated"}
            description={getLastUpdated(data)}
            className={"filter--component"}
          />
          <Card
            img={"http://#"}
            title={"Last Online"}
            description={getLastOnline(data)}
            className={"filter--component"}
          />

          <Card
            img={"http://#"}
            title={"Countries"}
            description={`We have clients from ${countCountries(
              data
            )} countries`}
            className={"filter--component"}
          />
        </Navbar>
        <div id="submenu">
          <input
            id="submenu--serial"
            placeholder="Search by sn"
            type="number"
            onChange={(e) => setSelectData(searchSerial(data, e.target.value))}
          />
          <div>
            <Dropdown setSelection={setSelection} />
          </div>
        </div>

        {/*
        Main purpose of this block is do demonstrate that we can inject some logic via parent component;
        It would be better to move it's own module
        */}
        <Modal open={isModalOpen}>
          {isCountryDataLoaded ? (
            <div className="contentModal">
              <h1>Some usefull data related to {countryInfo?.name}</h1>
              <ul>
                <li>Capital: {countryInfo?.capital}</li>
                <li>region: {countryInfo?.region}</li>
                <li>population: {countryInfo?.population}</li>
                <li>
                  flag: <img src={countryInfo?.flag} alt="china" width={50} />
                </li>
              </ul>
            </div>
          ) : (
            <h1>Fetching data</h1>
          )}
        </Modal>

        <DataTable
          dataProp={selectData}
          setIsModalOpen={setIsModalOpen}
          fetchCountryApi={fetchCountryApi}
        />
      </section>
    </div>
  );
};
