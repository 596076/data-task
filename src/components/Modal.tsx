import { Dispatch, SetStateAction } from "react";
import ReactDOM from "react-dom/client";
import { createPortal } from "react-dom";
import "./Modal.css";
interface IModal {
  children: JSX.Element;
  open: boolean;
}

export const Modal: React.FC<IModal> = ({ children, open }: IModal) => {
  if (!open) return null;
  return createPortal(
    <div id="modal-container">
      <div id="modal-wrapper">{children}</div>
    </div>,
    document.getElementById("mainRootModal")!
  );
};
