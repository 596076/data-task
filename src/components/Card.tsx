import React from "react";
import "./Card.css"
interface IProps{
    img:string,
    title:string,
    className?:string
    description:string

}
export const Card:React.FC<IProps>=(props:IProps)=>{
    const {img,title,className,description}= props
    return (
        <div className={className}>
            <div className='filter--body'>
                {title}
                <img src={img}/>
            </div>
            <div className='filter--body description'>
                {description}
            </div>
        </div>
    )
}