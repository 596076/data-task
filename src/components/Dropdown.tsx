import { Dispatch, SetStateAction } from "react";

interface IDrop {
  setSelection: Dispatch<SetStateAction<string>>;
}
export const Dropdown: React.FC<IDrop> = (props: IDrop) => {
  const { setSelection } = props;
  return (
    <>
      <label htmlFor="sorting">Sort by:</label>
      <select id="sorting" onChange={(e) => setSelection(e.target.value)}>
        <option value="serial">Serial number</option>
        <option value="online">Last online</option>
        <option value="updated">Last updated</option>
      </select>
    </>
  );
};
