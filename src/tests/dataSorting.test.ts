import { describe , expect, it } from 'vitest'
import {parseData,dataUnit,newUnit} from '../data/jsonDeserialize'
import {sortLastOnline,sortLastUpdated} from '../data/helpers'

const data:dataUnit[] = [  {
    "serialNumber":"35162491",
    "currentSoftwareVersion":"1.8.10017.12375",
    "lastUpdated":"2022-10-06T12:27:27.7666396Z",
    "level":"Error",
    "context":"{\"message\":\"TemperatureSensorError 'Disconnected'\"}",
    "countryCode":"NL",
    "productCountryCode":"NL",
    "productTags":";psu_version_a",
    "component":"Unknown",
    "lastOnline":"2022-10-10T00:00:00Z",
    "slidingHistoryFlakinessDetected":true
},
    {
        "serialNumber":"35228063",
        "currentSoftwareVersion":"1.8.10017.12375",
        "lastUpdated":"2022-10-05T14:59:46.2030184Z",
        "level":"Error",
        "context":"{\"message\":\"TemperatureSensorError 'Disconnected'\"}",
        "productCountryCode":"DE",
        "productTags":";psu_version_a",
        "component":"Unknown",
        "lastOnline":"2022-10-05T14:59:46.2030184Z",
        "slidingHistoryFlakinessDetected":false
    },
    {
        "serialNumber":"34511124",
        "currentSoftwareVersion":"1.8.10017.12375",
        "lastUpdated":"2022-10-05T11:43:01.4968122Z",
        "level":"Error",
        "context":"{\"message\":\"TemperatureSensorError 'Disconnected'\"}",
        "productCountryCode":"SG",
        "productTags":";psu_version_a;system.paireddeviceaccountowner",
        "component":"Unknown",
        "lastOnline":"2022-10-10T00:00:00Z",
        "slidingHistoryFlakinessDetected":true
    }]



// const result:newUnit[]=parseData()

describe("Sorting functions",()=>{
    const tempData:newUnit[] = data.map(element=> parseData(element))
    

    // [sortedArray[0].lastUpdated,sortedArray[1].lastUpdated,sortedArray[2].lastUpdated]
    it("should sort last updated in asc order",()=>{
        const sortedArray = [...sortLastUpdated(tempData)]
        const testArr = sortedArray.map(element=> element.lastUpdated.valueOf())
        expect(testArr).toStrictEqual([1664970181496,1664981986203,1665059247766])
    })


    it("should sort last online in asc order",()=>{
        const sortedArray = [...sortLastOnline(tempData)]
        const testArr = sortedArray.map(element=> element.lastOnline.valueOf())
        expect(testArr).toStrictEqual([1664981986203,1665360000000,1665360000000])
    })
})

