import React, {useState} from 'react';
import './App.css';
import {Main} from './components/Main'
import {Spinner} from './components/Spinner'
function App() {
    const [isLoading, setLoading] = useState(true)

    return (
        <div className="App">
            { isLoading && <Spinner /> }
            <Main setLoading={setLoading}/>

        </div>
    );
}

export default App;
